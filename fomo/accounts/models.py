from django.db import models
from django.contrib.auth.models import AbstractUser
class FOMOUser(AbstractUser):
    #username
    #password
    #email
    #first_name
    #last_name
    birthdate = models.DateField(null=True)
    gender = models.BooleanField(default=1)
    streetaddress = models.TextField(null=True)
    city =  models.TextField(null=True)
    state = models.TextField(null=True)
    postalcode = models.TextField(null=True)


    #is_staff
    #is_superuser
    #last_login
    #get_username()
    #get_full_name()
    #get_short_name()
    #check_password(raw_password)
    #create_user()
    #create_superuser()
    #user_logged_in()
    #user_logged_out()
    #get_user_permissions()
    #user_can_authenticate
