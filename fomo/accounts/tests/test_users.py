from django.test import TestCase
from accounts import models as amod
from datetime import datetime

class FOMOUserTestCase(TestCase):
    def test_add_fomousers(self):
        fomouser1 = amod.FOMOUser()
        fomouser1.username = "blake"
        fomouser1.set_password( "batchelor")
        fomouser1.email = "blakebatchelor@test.com"
        fomouser1.first_name = "Blake"
        fomouser1.last_name = "Batchelor"
        fomouser1.birthdate = datetime.now()
        fomouser1.gender = 1
        fomouser1.streetaddress = "123 easy street"
        fomouser1.city = "T and C"
        fomouser1.state = "New Mexico"
        fomouser1.postalcode = "87104"
        fomouser1.is_staff = 1
        fomouser1.is_superuser = 1
        fomouser1.last_login = datetime.now()
        fomouser1.save()

        fomouser2 = amod.FOMOUser.objects.get(id=fomouser1.id)
        self.assertEqual(fomouser1.first_name, fomouser2.first_name)
        self.assertEqual(fomouser1.last_name, fomouser2.last_name)
        self.assertEqual(fomouser1.gender, fomouser2.gender)
        self.assertEqual(fomouser1.streetaddress, fomouser2.streetaddress)
        self.assertEqual(fomouser1.city, fomouser2.city)
        self.assertEqual(fomouser1.state, fomouser2.state)
        self.assertEqual(fomouser1.postalcode, fomouser2.postalcode)
        self.assertEqual(fomouser1.is_staff, fomouser2.is_staff)
        self.assertEqual(fomouser1.is_superuser, fomouser2.is_superuser)

    def test_edit_fomousers(self):
        fomouser1 = amod.FOMOUser()
        fomouser1.username = "blake"
        fomouser1.set_password( "batchelor")
        fomouser1.email = "blakebatchelor@test.com"
        fomouser1.first_name = "Blake"
        fomouser1.last_name = "Batchelor"
        fomouser1.gender = 1
        fomouser1.streetaddress = "123 easy street"
        fomouser1.city = "T and C"
        fomouser1.state = "New Mexico"
        fomouser1.postalcode = "87104"
        fomouser1.is_staff = 1
        fomouser1.is_superuser = 1
        fomouser1.last_login = datetime.now()
        fomouser1.save()

        fomouser2 = amod.FOMOUser.objects.get(id=fomouser1.id)
        fomouser2.username = "bke"
        fomouser2.email = "blatchelor@test.com"
        fomouser2.first_name = "ke"
        fomouser2.last_name = "Balor"
        fomouser2.gender = 1
        fomouser2.streetaddress = "123y street"
        fomouser2.city = "T an"
        fomouser2.state = "New Mico"
        fomouser2.postalcode = "871e4"
        fomouser2.is_staff = 0
        fomouser2.is_superuser = 0
        fomouser2.save()

        fomouser3 = amod.FOMOUser.objects.get(id=fomouser1.id)
        self.assertEqual(fomouser3.first_name, fomouser2.first_name)
        self.assertEqual(fomouser3.last_name, fomouser2.last_name)
        self.assertEqual(fomouser3.birthdate, fomouser2.birthdate)
        self.assertEqual(fomouser3.gender, fomouser2.gender)
        self.assertEqual(fomouser3.streetaddress, fomouser2.streetaddress)
        self.assertEqual(fomouser3.city, fomouser2.city)
        self.assertEqual(fomouser3.state, fomouser2.state)
        self.assertEqual(fomouser3.postalcode, fomouser2.postalcode)
        self.assertEqual(fomouser3.is_staff, fomouser2.is_staff)
        self.assertEqual(fomouser3.is_superuser, fomouser2.is_superuser)

    def test_delete_fomousers(self):
        fomouser1 = amod.FOMOUser()
        fomouser1.username = "blake"
        fomouser1.set_password( "batchelor")
        fomouser1.email = "blakebatchelor@test.com"
        fomouser1.first_name = "Blake"
        fomouser1.last_name = "Batchelor"
        fomouser1.birthdate = datetime.now()
        fomouser1.gender = 1
        fomouser1.streetaddress = "123 easy street"
        fomouser1.city = "T and C"
        fomouser1.state = "New Mexico"
        fomouser1.postalcode = "87104"
        fomouser1.is_staff = 1
        fomouser1.is_superuser = 1
        fomouser1.last_login = datetime.now()
        fomouser1.save()

        fomouser1.delete()

        emptyuser = amod.FOMOUser.objects.filter(id=fomouser1.id)
        self.assertEqual(len(emptyuser), 0)

    def test_changepassword(self):
        fomouser1 = amod.FOMOUser()
        fomouser1.username = "blake"
        fomouser1.set_password( "batchelor")
        fomouser1.email = "blakebatchelor@test.com"
        fomouser1.first_name = "Blake"
        fomouser1.last_name = "Batchelor"
        fomouser1.birthdate = datetime.now()
        fomouser1.gender = 1
        fomouser1.streetaddress = "123 easy street"
        fomouser1.city = "T and C"
        fomouser1.state = "New Mexico"
        fomouser1.postalcode = "87104"
        fomouser1.is_staff = 1
        fomouser1.is_superuser = 1
        fomouser1.last_login = datetime.now()
        fomouser1.save()

        passwordhash = fomouser1.password

        fomouser2 = amod.FOMOUser.objects.get(id=fomouser1.id)
        fomouser2.set_password('it worked')
        fomouser2.save()

        self.assertNotEqual(fomouser2.password, passwordhash)
