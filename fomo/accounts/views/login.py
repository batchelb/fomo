from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login
from accounts import models as amod

@view_function
def process_request(request):
    print(">>>>> got to login.py")
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print(form)
        if form.is_valid():
            login(request,form.cleaned_data.get('user'))

            #go to the my accounts padding-left
            return HttpResponseRedirect('/accounts/index')



    else:
        form = LoginForm()
    return dmp_render(request, 'login.html', {'form': form,})   # hey

#################################33login modal

@view_function
def modal(request):

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            login(request,form.cleaned_data.get('user'))

            #go to the my accounts padding-left
            return HttpResponse('''
                <script>
                    window.location.href = '/homepage/index/'
                </script>
            ''')



    else:
        form = LoginForm()

    return dmp_render(request, 'login.modal.html', {'form': form,})   # hey



class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label='Password', max_length=100, widget = forms.PasswordInput())

    def clean_username(self):
        username = self.cleaned_data['username']
        return username
        ###check if username duplicate = amod.FOMOUser.objects.filter(username = username)

    def clean_password(self):
        password = self.cleaned_data['password']
        return password

    def clean(self):

        #do authenticate he
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        print(username +'|'+ password)
        user = authenticate(username=username,password=password)

        if user is None:
            print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
            raise forms.ValidationError('Invalid username or password')

        self.cleaned_data['user'] = user

        return self.cleaned_data
