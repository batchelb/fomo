from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from accounts import models as amod
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import authenticate, login
from django.forms.widgets import TextInput


@view_function
def process_request(request):


    if request.method == 'POST':
        form=addFOMOUserForm(request.POST)
        form.Request = request
        if form.is_valid():
            user = amod.FOMOUser()
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.username = form.cleaned_data['username']
            user.set_password(form.cleaned_data['password'])
            user.email = form.cleaned_data['email']
            user.streetaddress = form.cleaned_data['streetaddress']
            user.city = form.cleaned_data['city']
            user.state = form.cleaned_data['state']
            user.postalcode = form.cleaned_data['postalcode']
            user.gender = form.cleaned_data['gender']
            user.birthdate = form.cleaned_data['birthdate']
            user.save()

            print(1)
            login(request,authenticate(username=form.cleaned_data['username'],password=form.cleaned_data['password']))
            return HttpResponseRedirect('/accounts/signupsuccessful')
        else:
            return dmp_render(request,'signup.html',{'form' : form,})
    else:

        form = addFOMOUserForm()



        return dmp_render(request,'signup.html',{'form' : form,})



class addFOMOUserForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)
    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label='Password', max_length=100, widget = forms.PasswordInput())
    email = forms.CharField(label='Email', max_length=100)
    streetaddress = forms.CharField(label='Street Address', max_length=100, required=False)
    city = forms.CharField(label='city', max_length=100, required=False)
    state = forms.CharField(label='state', max_length=100, required=False)
    postalcode = forms.CharField(label='postalcode', max_length=100, required=False)
    gender = forms.ChoiceField(label='Gender', required=False, choices=[['True','Male'],['False','Female']])
    birthdate = forms.DateField(label='Birthdate', required=False)
    Request = None

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        return first_name
    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        return last_name
    def clean_username(self):
        username = self.cleaned_data.get('username')
        return username
    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email
    def clean_streetaddress(self):
        streetaddress = self.cleaned_data.get('streetaddress')
        return streetaddress
    def clean_city(self):
        city = self.cleaned_data.get('city')
        return city
    def clean_state(self):
        reorder_state = self.cleaned_data.get('reorder_state')
        return reorder_state
    def clean_postalcode(self):
        postalcode = self.cleaned_data.get('postalcode')
        return postalcode
    def clean_birthdate(self):
        birthdate = self.cleaned_data.get('birthdate')
        return birthdate
    def clean_gender(self):
        gender = self.cleaned_data.get('gender')
        return gender
    def clean(self):
        requestedusername = self.cleaned_data.get('username')
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        if len(amod.FOMOUser.objects.filter(username = requestedusername).exclude(id=self.Request.user.id)) > 0:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('stop')
            raise forms.ValidationError('username already taken')

        return self.cleaned_data
