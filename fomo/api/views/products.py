from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from django import forms
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, HttpResponseNotFound, HttpResponseServerError
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required


@view_function
def process_request(request):
    name = request.GET.get('name')
    minprice = request.GET.get('minprice')
    maxprice = request.GET.get('maxprice')
    categoryname =  request.GET.get('categoryname')

    ret = {
    'Products':[]
    }



    form = ProductForm(request.GET)
    form.Request = request

    print('hererere')
    if form.is_valid():
        qry = cmod.Product.objects
        if len(request.GET)==0:
            qry = qry.all()
        else:
            if name:
                qry = qry.filter(name__icontains=name)
            if minprice:
                qry = qry.filter(price__gte=minprice)
            if maxprice:
                qry = qry.filter(price__lte=maxprice)
            if categoryname:
                qry = qry.filter(category__name__icontains=categoryname)
        for p in qry:
            if hasattr(p, 'serial_number'):
                ret.get('Products').append({
                    'name':p.name,
                    'catgory': p.category.name,
                    'price':p.price,
                    'create_date':p.create_date,
                    'description':p.description,
                    'serial_number':p.serial_number,
                })
            else:
                ret.get('Products').append({
                    'name':p.name,
                    'catgory': p.category.name,
                    'price':p.price,
                    'create_date':p.create_date,
                    'description':p.description,
                    'quantity':p.quantity,
                })



        return JsonResponse(ret)
    return HttpResponseServerError(form.badtext)

class ProductForm(forms.Form):
    Product_Choices = [
        ['unique','Serialized Product'],
        ['bulk', 'Bulk Product'],
        ['rental','Rental Product']
    ]

    name = forms.CharField(label='Product Name', max_length=100,required=False)
    categoryname = forms.CharField(label='Category',required=False)
    minprice = forms.CharField(label='Price',required=False)
    maxprice = forms.CharField(label='Price',required=False)
    badtext = ""
    Request=None



    def clean_name(self):
        name = self.cleaned_data.get('name')
        return name

    def clean_minprice(self):
        minprice = self.cleaned_data.get('minprice')

        if self.isfloat(minprice) or minprice == "":
            return minprice
        else:
            self.badtext = self.badtext + "Min price must be an int "
            raise forms.ValidationError("Min price must be an int ")

    def clean_maxprice(self):
        maxprice = self.cleaned_data.get('maxprice')
        if self.isfloat(maxprice) or maxprice == "":
            return maxprice
        else:
            self.badtext = self.badtext + "Max price must be an int "
            raise forms.ValidationError("Max price must be an int ")

    def clean_categoryname(self):

        categoryname = self.cleaned_data.get('categoryname')
        return categoryname


    def clean(self):
        print(len(self.Request.GET))
        if len(self.Request.GET) == 0 or (self.Request.GET.get('name')  or self.Request.GET.get('categoryname')  or self.Request.GET.get('minprice')  or self.Request.GET.get('maxprice')):
            return self.cleaned_data

        else:
            self.badtext = self.badtext + "Please complete the parameter"
            raise forms.ValidationError("Please include one parameter")

    def isfloat(self,x):
        try:
            a = float(x)
            return True
        except ValueError:
            return False
