from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from . import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required




def last5products(request):

    ### get last5 products out of the database
    holding = cmod.Product.objects.all().filter(id__in=request.last5)
    last5 = []

    ##sourt them in order of the last 5
    if len(holding) > 0:
        for y in range(0,len(request.last5)):
            for x in range(0,len(holding)):
                if(holding[x].id == request.last5[y]):
                        last5.insert(y,holding[x])

    #return last5
    return last5


def last5pictures(last5):
    last5pictures = []


    for p in last5:
        try:
            ##get only one picture for each of the last 5
            productpicture = cmod.ProductPictures.objects.filter(product__id=p.id)[:1].get()
            last5pictures.append(productpicture)
        except cmod.Product.DoesNotExist:
            pass
            last5pictures.append('not there')

    return last5pictures
