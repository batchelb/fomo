# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-22 23:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_productcart_viewedlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='productcart',
            name='date_added',
            field=models.DateTimeField(auto_now_add=True, default='2017-03-22 17:48:40'),
            preserve_default=False,
        ),
    ]
