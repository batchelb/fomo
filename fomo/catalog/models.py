from django.db import models
from polymorphic.models import PolymorphicModel
from accounts import models as amod
# Create your models here.
# we use composition which is taking sub objects and two serperate object with foriegn key between them, liek we have been doing
# regular inheratinace data split between tables and can create suer class, pk is fk
#  abstract inheratune  is like ruglar but cannot create supper class

class Category(models.Model):
    #id the id will be set on the save
    codename = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

class Product(PolymorphicModel):
    #id
    name = models.TextField(blank=True, null=True)
    category = models.ForeignKey(Category) #ForeignKey goes on the many side of the relationship
    price = models.DecimalField(max_digits=8, decimal_places=2) # 999,999.99
    brand = models.TextField(null=True)
    create_date = models.DateTimeField(auto_now_add = True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

class ProductPictures(models.Model):
    product = models.ForeignKey(Product)
    filepath = models.TextField(blank=True, null=True)

class BulkProduct(Product):
    #id
    #name
    #cateogry
    #price
    #create_date
    # product = models.OneToOneField('Product')
    quantity = models.IntegerField()
    reorder_trigger = models.IntegerField()
    reorder_quantity = models.IntegerField()


class UniqueProduct(Product):
    #id
    #name
    #cateogry
    #price
    #create_date
    # product = models.OneToOneField('Product')
    serial_number = models.TextField()
    condition = models.TextField()


class RentalProduct(Product):
    #id
    #name
    #cateogry
    #price
    #create_date
    # product = models.OneToOneField('Product')
    serial_number = models.TextField()
    condition = models.TextField()


class ProductCart(models.Model):
    user = models.ForeignKey(amod.FOMOUser)
    product = models.ForeignKey(Product)
    active = models.BooleanField(default=True)
    quantity = models.IntegerField(default=1)
    date_added = models.DateTimeField(auto_now_add=True)


class ViewedLog(models.Model):
    user = models.ForeignKey(amod.FOMOUser)
    product = models. ForeignKey(Product)
    date_viewed = models.DateTimeField(auto_now_add=True)
