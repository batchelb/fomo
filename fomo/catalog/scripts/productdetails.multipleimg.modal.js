$(function(){
    let productimgs = $('.productimages');
    let icount = 0;
    $(productimgs[icount]).toggleClass('hide')
    $('#jquery-loadmodal-js-title').html('Product Images')

    $('#next').click(function(){

        $(productimgs[icount]).toggleClass('hide');

        if(icount >= productimgs.length-1)
        {
            icount= 0;
        }
        else
        {
            icount = icount + 1;
        }
        $(productimgs[icount]).toggleClass('hide');

    });

    $('#previous').click(function()
    {

        $(productimgs[icount]).toggleClass('hide');

        if(icount > 0)
        {
            icount = icount - 1;
        }
        else
        {
            icount = productimgs.length-1
        }
        console.log(icount)
        $(productimgs[icount]).toggleClass('hide');

    });

});
