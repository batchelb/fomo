from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required
from catalog import cataloghelper

@view_function
def process_request(request):
    productfilter = request.urlparams[0]

    if productfilter == "":
        #query all products
        products = cmod.Product.objects.order_by('name').all()
        productpicture = cmod.ProductPictures.objects.all()
        category = "All Products"

    else:
        products = cmod.Product.objects.order_by('name').filter(category__codename = productfilter)

        productpicture = cmod.ProductPictures.objects.filter(product__category__codename=productfilter)

        category = cmod.Category.objects.filter(codename=productfilter)[:1].get().name

    for p in products:
        p.filepaths = []
        for pic in productpicture:
            if pic.product.id == p.id:
                p.filepaths.append(pic.filepath)
                print(p.filepaths[0])


    categories = cmod.Category.objects.order_by('name').all()

    ##get last5 products
    last5 = cataloghelper.last5products(request)

    ##get last 5 pictures
    last5pictures = cataloghelper.last5pictures(last5)

    context = {
    'products': products,
    'last5': last5,
    'categories' : categories,
    'last5pictures' : last5pictures,
    'category':category
    }

    return dmp_render(request, 'index.html', context)
