from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required
from catalog import cataloghelper

@view_function
def process_request(request):

    #get the current product
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    holdingimgcontainer = cmod.ProductPictures.objects.filter(product__id=request.urlparams[0])
    product.filepaths = []

    for img in holdingimgcontainer:
        product.filepaths.insert(0,img)

    categories = cmod.Category.objects.order_by('name').all()


##############Start last five code
# see if the product is in last5 already if it is remove
    if product.id in request.last5:
        request.last5.remove(product.id)

# add product back at the beginning
    request.last5.insert(0,product.id)

# remove if request .last 5 has more than 5
    while len(request.last5) > 5:
        request.last5.pop()

    ##get last5 products
    last5 = cataloghelper.last5products(request)

    ##get last 5 pictures
    last5pictures = cataloghelper.last5pictures(last5)


#return the product and last5
    context = {
        'product': product,
        'last5': last5,
        'last5pictures': last5pictures,
        'categories' : categories
    }
    return dmp_render(request, 'productdetails.html', context)






################################
@view_function
def multipleimg(request):
    productimgs = cmod.ProductPictures.objects.all().filter(product__id=request.urlparams[0])

    return dmp_render(request,'productdetails.multipleimg.modal.html',{'productimgs':productimgs})
