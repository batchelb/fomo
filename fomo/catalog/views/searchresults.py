from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required
from catalog import cataloghelper

@view_function
def process_request(request):

    #Get search text
    searchtext = request.GET.get('search-text')

    ###if user searched for products
    if searchtext is not None:
        products = cmod.Product.objects.order_by('name').filter(name__icontains = searchtext) | cmod.Product.objects.order_by('name').filter(price__icontains = searchtext) | cmod.Product.objects.order_by('name').filter(category__name__icontains = searchtext) | cmod.Product.objects.order_by('name').filter(brand__icontains = searchtext) | cmod.Product.objects.order_by('name').filter(description__icontains = searchtext) | cmod.Product.objects.order_by('name').filter(uniqueproduct__serial_number__icontains = searchtext)
        productpicture = cmod.ProductPictures.objects.all()

        if len(products)>=1:
            category = "Search results for \"" + searchtext+ "\""
        else:
            category = "No search results for \"" + searchtext+ "\""

        for p in products:
            p.filepaths = []
            for pic in productpicture:
                if pic.product.id == p.id:
                    p.filepaths.append(pic.filepath)

        categories = cmod.Category.objects.order_by('name').all()

        ##get last5 products
        last5 = cataloghelper.last5products(request)

        ##get last 5 pictures
        last5pictures = cataloghelper.last5pictures(last5)

        context = {
        'products': products,
        'last5': last5,
        'categories' : categories,
        'last5pictures' : last5pictures,
        'category':category
        }

        return dmp_render(request, 'searchresults.html', context)
    else:
        return HttpResponseRedirect('/catalog/index/')
