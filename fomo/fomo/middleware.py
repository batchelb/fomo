#from catalog import somefunction

def lastFiveMiddleware(get_response):


    def middleware(request):

        #get current last5 from the session
        last5list = request.session.get('last5')

        #if non set to empty array
        if last5list is None:
            last5list = []

        #attach to the request object
        request.last5 = last5list


        response = get_response(request)


        #save to function
        request.session['last5'] = request.last5



        return response

    return middleware
