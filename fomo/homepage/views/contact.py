from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect

@view_function
def process_request(request):
    print(">>>>> got to contact.py")
    if request.method == 'POST':
        if form.is_valid():
            print('Vaild')

            form_name = form_cleaned_data.get('name')
            form_email = form_cleaned_data.get('email')
            form_message = form_cleaned_data.get('message')
            sendmail(
                'form_name',
                form_message,
                form_email,
                ['blakebatchelor2@gmail.com'],
                fail_silently=False,
            )
            return HttpResponseRedirect('/')


    else:
        form = ContactForm()

    return dmp_render(request, 'contact.html', {'form': form,})   # hey




class ContactForm(forms.Form):
    name = forms.CharField(label='Full Name', max_length=100)
    email = forms.EmailField(label='Email', max_length=100)
    message = forms.CharField(label = 'Message', max_length=1000,
        widget =forms.Textarea(attrs={'class': 'form-control'})
    )

    def clean_name(self):
        name = self.cleaned_data.get('name')
        parts = name.strip().split()
        if len(parts) <= 1:
            raise forms.ValidationError('Please enter both first and last name')
        return name
