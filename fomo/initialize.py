import os

os.environ['DJANGO_SETTINGS_MODULE']='fomo.settings'
import django
django.setup()
#ask if the user has a specific, exact permission
#like if(user can create a pruduct){}
from django.contrib.auth.models import Permission, Group
#p.cpde_name is options
#permisoins get carried out decarator such as @login_required @permission_required('accounts.contactus #view for security', raise_exception=true #takes to a 403)

from datetime import datetime
from decimal import Decimal
from catalog import models as cmod
from accounts import models as amod
from django.contrib.contenttypes.models import ContentType

#create permission
content_type = ContentType.objects.get_for_model(amod.FOMOUser)

# permission = Permission.objects.create(
#     codename ='add_product',
#     name ='Add a new product to our system',
#     content_type = content_type,
#
# )
# permission1 = Permission.objects.create(
#     codename='change_product',
#     name='change product information',
#     content_type = content_type,
#
# )
permission2 = Permission.objects.create(
    codename='see_products',
    name='see list of products',
    content_type = content_type,

)
# permission3 = Permission.objects.create(
#     codename='delete_product',
#     name='delete a user from our system',
#     content_type = content_type,
#
# )
#
permission4 = Permission.objects.create(
    codename='see_fomousers',
    name='see all users from our system',
    content_type = content_type,

)

# permissions = Permission.objects.all()
# for p in permissions:
#     print(p)
# #create group
g1 = Group ()
g1.name = 'SalesPeople'
g1.save()
g1.permissions.add(Permission.objects.get(codename='add_fomouser'))
g1.permissions.add(Permission.objects.get(codename='change_fomouser'))
g1.permissions.add(Permission.objects.get(codename='delete_fomouser'))
g1.permissions.add(Permission.objects.get(codename='see_fomousers'))
g1.save()

g2 = Group()
g2.name = 'SuperUser'
g2.save()
g2.permissions.add(Permission.objects.get(codename='add_fomouser'))
g2.permissions.add(Permission.objects.get(codename='change_fomouser'))
g2.permissions.add(Permission.objects.get(codename='delete_fomouser'))
g2.permissions.add(Permission.objects.get(codename='add_product'))
g2.permissions.add(Permission.objects.get(codename='change_product'))
g2.permissions.add(Permission.objects.get(codename='delete_product'))
g2.permissions.add(Permission.objects.get(codename='see_fomousers'))
g2.permissions.add(Permission.objects.get(codename='see_products'))
g2.save()

g3 = Group()
g3.name = 'Inventory'
g3.save()
g3.permissions.add(Permission.objects.get(codename='add_product'))
g3.permissions.add(Permission.objects.get(codename='change_product'))
g3.permissions.add(Permission.objects.get(codename='delete_product'))
g3.permissions.add(Permission.objects.get(codename='see_products'))
g3.save()



print('creating 1st user object')
fomouser1 = amod.FOMOUser()
fomouser1.username = "blake"
fomouser1.set_password( "batchelor")
fomouser1.email = "blakebatchelor@test.com"
fomouser1.first_name = "Blake"
fomouser1.last_name = "Batchelor"
fomouser1.birthdate = datetime.now()
fomouser1.gender = 1
fomouser1.streetaddress = "123 easy street"
fomouser1.city = "T and C"
fomouser1.state = "New Mexico"
fomouser1.postalcode = "87104"
fomouser1.is_staff = 1
fomouser1.is_superuser = 1
fomouser1.last_login = datetime.now()
fomouser1.save()

# #add group permission
fomouser1.groups.add(g2)
fomouser1.save()
#
#


print('creating 2st user object')
fomouser2 = amod.FOMOUser()
fomouser2.username  = "kenyon"
fomouser2.set_password( "anderson")
fomouser2.email = "kenyonanderson@test.com"
fomouser2.first_name = "Kenyon"
fomouser2.last_name = "Anderson"
fomouser2.birthdate = datetime.now()
fomouser2.gender = 1
fomouser2.streetaddress = "456 moderate street"
fomouser2.city = "Provo"
fomouser2.state = "UT"
fomouser2.postalcode = "86757"
fomouser2.is_staff = 1
fomouser2.is_superuser =0
fomouser2.last_login = datetime.now()
fomouser2.save()
fomouser2.groups.add(g1)
fomouser2.save()


print('creating 3st user object')
fomouser3 = amod.FOMOUser()
fomouser3.username = "nate"
fomouser3.set_password( "shawcroft")
fomouser3.email = "nateshawcroft@test.com"
fomouser3.first_name = "Nate"
fomouser3.last_name = "Shawcroft"
fomouser3.birthdate = datetime.now()
fomouser3.gender = 1
fomouser3.streetaddress = "789 Hard street"
fomouser3.city = "Flying Dragon"
fomouser3.state = "MT"
fomouser3.postalcode = "94950"
fomouser3.is_staff = 1
fomouser3.is_superuser = 0
fomouser3.last_login = datetime.now()
fomouser3.save()

fomouser3.groups.add(g3)
fomouser3.save()

print('creating 4st user object')
fomouser4 = amod.FOMOUser()
fomouser4.set_password( "christensen")
fomouser4.email = "frederikchristensen@test.com"
fomouser4.first_name = "Frederik"
fomouser4.last_name = "Christensen"
fomouser4.birthdate = datetime.now()
fomouser4.gender = 1
fomouser4.streetaddress = "012 homework stinks"
fomouser4.city = "Rockie Mountains"
fomouser4.state = "Idaho"
fomouser4.postalcode = "75968"
fomouser4.is_staff = 0
fomouser4.is_superuser = 0
fomouser4.last_login = datetime.now()
fomouser4.save()

p = Permission.objects.get(codename='add_fomouser')
fomouser4.user_permissions.add(p)
fomouser4.save()

#create a category
cat1 = cmod.Category()
cat1.codename = 'kids'
cat1.name='Kids Toy Products'
cat1.save()

#create a category
cat2 = cmod.Category()
cat2.codename = 'brass'
cat2.name='Brass Insturments'
cat2.save()

#create a category
cat3 = cmod.Category()
cat3.codename = 'woodwind'
cat3.name='Woodwind'
cat3.save()

#create a category
cat4 = cmod.Category()
cat4.codename = 'percussion'
cat4.name='Percussion Instrument'
cat4.save()

cat5 = cmod.Category()
cat5.codename = 'string'
cat5.name='String Instrument'
cat5.save()

#create a Bulkproduct
p1 = cmod.BulkProduct()
p1.name ="Small Fiddle"
p1.category= cat5
p1.price = Decimal('39.50')
p1.brand = "Yahma"
p1.quantity = 20
p1.reorder_trigger = 10
p1.reorder_quantity = 10
p1.description = 'A wonderful fiddle for small children ages 3-6. It is made out of a durable plastic that can with stand even the most energetic child. It is pre-tuned to make it sound wonderful.'
p1.save()

#create a Bulkproduct
p2 = cmod.BulkProduct()
p2.name ="Small Trumpet"
p2.category= cat2
p2.price = Decimal('34.50')
p2.brand = "Yahma"
p2.quantity = 20
p2.reorder_trigger = 10
p2.reorder_quantity = 10
p2.description = p1.description = 'A wonderful trumpet for small children ages 3-6. It is made out of a durable plastic that can with stand even the most energetic child. It is pre-tuned to make it sound wonderful.'
p2.save()

#create a Bulkproduct
p3 = cmod.BulkProduct()
p3.name ="Small Drum"
p3.category= cat4
p3.price = Decimal('19.50')
p3.brand = "Yahma"
p3.quantity = 20
p3.reorder_trigger = 10
p3.reorder_quantity = 10
p3.description = 'A wonderful drum for small children ages 3-6. It is made out of a durable plastic that can with stand even the most energetic child. It is pre-tuned to make it sound wonderful.'
p3.save()

#create a Bulkproduct
p4 = cmod.BulkProduct()
p4.name ="Small Flute"
p4.category= cat3
p4.price = Decimal('19.50')
p4.brand = "Yahma"
p4.quantity = 20
p4.reorder_trigger = 10
p4.reorder_quantity = 10
p4.description = 'A wonderful flute for small children ages 3-6. It is made out of a durable plastic that can with stand even the most energetic child. It is pre-tuned to make it sound wonderful.'
p4.save()

#create a Uniqueproduct
p5 = cmod.UniqueProduct()
p5.name ="Flute"
p5.category= cat3
p5.price = Decimal('199.50')
p5.brand = "Yahma"
p5.serial_number = "1234h567"
p5.condition = "new"
p5.description = 'A wonderful instrument for everyone. It is made out of a titanium alloy that makes the crispest noise. It is pre-tuned to make it sound wonderful.'
p5.save()

#create aUniquekproduct
p6 = cmod.UniqueProduct()
p6.name ="Drum"
p6.category= cat4
p6.price = Decimal('199.50')
p6.brand = "Yahma"
p6.serial_number = "1234h567"
p6.condition = "new"
p6.description = 'A wonderful drum for everyone. It is made out of a cedar that makes the crispest noise. It is pre-tuned to make it sound wonderful.'
p6.save()

#create a Uniqueproduct
p7 = cmod.UniqueProduct()
p7.name ="Flute"
p7.category= cat3
p7.price = Decimal('199.50')
p7.brand = "Yahma"
p7.serial_number = "1234g67"
p7.condition = "Used"
p7.description = 'A wonderful flute for everyone. It is made out of a titanium alloy that makes the crispest noise. It is pre-tuned to make it sound wonderful.'
p7.save()

#create a Uniqueproduct
p8 = cmod.UniqueProduct()
p8.name ="Fiddle"
p8.category= cat5
p8.price = Decimal('19.50')
p8.brand = "Yahma"
p8.serial_number = "123f4567"
p8.condition = "new"
p8.description = 'A wonderful flute for everyone. It is made out of a cedar that makes the crispest noise. It is pre-tuned to make it sound wonderful.'
p8.save()

#create a Rentalproduct
p9 = cmod.RentalProduct()
p9.name ="Flute"
p9.category= cat3
p9.price = Decimal('19.50')
p9.brand = "Yahma"
p9.serial_number = "1234d567"
p9.condition = "new"
p9.description = 'A wonderful flute for everyone. It is made out of a titanium alloy that makes the crispest noise. It is pre-tuned to make it sound wonderful. Plus it is affordable as a rental flute'
p9.save()

#create a Rentalproduct
p10 = cmod.RentalProduct()
p10.name ="Fiddle"
p10.category= cat5
p10.price = Decimal('19.50')
p10.brand = "Yahma"
p10.serial_number = "12a34567"
p10.condition = "new"
p10.description = 'A wonderful fiddle for everyone. It is made out of a cedar that makes the crispest noise. It is pre-tuned to make it sound wonderful. Plus it is affordable as a rental fiddle'
p10.save()

#create a Rentalproduct
p11 = cmod.RentalProduct()
p11.name ="Drum"
p11.category= cat4
p11.price = Decimal('19.50')
p11.brand = "Yahma"
p11.serial_number = "1a234567"
p11.condition = "new"
p11.description = 'A wonderful drum for everyone. It is made out of a cedar that makes the crispest noise. It is pre-tuned to make it sound wonderful. Plus it is affordable as a rental drum'
p11.save()

#create a Rentalproduct
p12 = cmod.RentalProduct()
p12.name ="Dragon"
p12.category= cat5
p12.price = Decimal('1999.50')
p12.brand = "Yahma"
p12.serial_number = "123wa4567"
p12.condition = "new"
p12.description = 'A wonderful dragon for everyone. It is made out of a pure magic that makes the crispest roar. It is breed to make it sound wonderful. Plus it is affordable as a rental.'
p12.save()

dragon = cmod.ProductPictures()
dragon.filepath = 'homepage/media/instrumentphotos/dragon.jpg'
dragon.product = p12
dragon.save()

Drumb = cmod.ProductPictures()
Drumb.filepath = 'homepage/media/instrumentphotos/drums.jpg'
Drumb.product = p11
Drumb.save()

fiddle = cmod.ProductPictures()
fiddle.filepath = 'homepage/media/instrumentphotos/fiddle.jpg'
fiddle.product = p10
fiddle.save()

flute = cmod.ProductPictures()
flute.filepath = 'homepage/media/instrumentphotos/flute.jpg'
flute.product = p9
flute.save()

fiddle2 = cmod.ProductPictures()
fiddle2.filepath = 'homepage/media/instrumentphotos/fiddle.jpg'
fiddle2.product = p8
fiddle2.save()

flute1 = cmod.ProductPictures()
flute1.filepath = 'homepage/media/instrumentphotos/flute.jpg'
flute1.product = p7
flute1.save()

Drumb1 = cmod.ProductPictures()
Drumb1.filepath = 'homepage/media/instrumentphotos/drums.jpg'
Drumb1.product = p6
Drumb1.save()

flute2 = cmod.ProductPictures()
flute2.filepath = 'homepage/media/instrumentphotos/flute.jpg'
flute2.product = p5
flute2.save()

flute3 = cmod.ProductPictures()
flute3.filepath = 'homepage/media/instrumentphotos/flute.jpg'
flute3.product = p4
flute3.save()

Drumb2 = cmod.ProductPictures()
Drumb2.filepath = 'homepage/media/instrumentphotos/drums.jpg'
Drumb2.product = p3
Drumb2.save()

trumpet = cmod.ProductPictures()
trumpet.filepath = 'homepage/media/instrumentphotos/trumpet.jpg'
trumpet.product = p2
trumpet.save()

violin2 = cmod.ProductPictures()
violin2.filepath = 'homepage/media/instrumentphotos/violin.jpg'
violin2.product = p1
violin2.save()


####################################################################3

violin3 = cmod.ProductPictures()
violin3.filepath = 'homepage/media/instrumentphotos/violin2.jpg'
violin3.product = p1
violin3.save()

trumpet2 = cmod.ProductPictures()
trumpet2.filepath = 'homepage/media/instrumentphotos/lemurtrumpet.jpg'
trumpet2.product = p2
trumpet2.save()

Drumb3 = cmod.ProductPictures()
Drumb3.filepath = 'homepage/media/instrumentphotos/drums2.jpg'
Drumb3.product = p3
Drumb3.save()

flute4 = cmod.ProductPictures()
flute4.filepath = 'homepage/media/instrumentphotos/flute2.jpg'
flute4.product = p4
flute4.save()

flute2 = cmod.ProductPictures()
flute2.filepath = 'homepage/media/instrumentphotos/flute2.jpg'
flute2.product = p5
flute2.save()

Drumb4 = cmod.ProductPictures()
Drumb4.filepath = 'homepage/media/instrumentphotos/drums2.jpg'
Drumb4.product = p6
Drumb4.save()

flute5 = cmod.ProductPictures()
flute5.filepath = 'homepage/media/instrumentphotos/flute2.jpg'
flute5.product = p7
flute5.save()

fiddle3 = cmod.ProductPictures()
fiddle3.filepath = 'homepage/media/instrumentphotos/violin2.jpg'
fiddle3.product = p8
fiddle3.save()

flute6 = cmod.ProductPictures()
flute6.filepath = 'homepage/media/instrumentphotos/flute2.jpg'
flute6.product = p9
flute6.save()

fiddle = cmod.ProductPictures()
fiddle.filepath = 'homepage/media/instrumentphotos/violin2.jpg'
fiddle.product = p10
fiddle.save()

Drumb5 = cmod.ProductPictures()
Drumb5.filepath = 'homepage/media/instrumentphotos/drums2.jpg'
Drumb5.product = p11
Drumb5.save()

dragon2 = cmod.ProductPictures()
dragon2.filepath = 'homepage/media/instrumentphotos/dragon2.png'
dragon2.product = p12
dragon2.save()

Drumb6 = cmod.ProductPictures()
Drumb6.filepath = 'homepage/media/instrumentphotos/drum-set.jpg'
Drumb6.product = p11
Drumb6.save()

Drumb7 = cmod.ProductPictures()
Drumb7.filepath = 'homepage/media/instrumentphotos/drum-set.jpg'
Drumb7.product = p6
Drumb7.save()

Drumb8 = cmod.ProductPictures()
Drumb8.filepath = 'homepage/media/instrumentphotos/drum-set.jpg'
Drumb8.product = p3
Drumb8.save()

viewlog = cmod.ViewedLog()
viewlog.user = fomouser1
viewlog.product = p1
viewlog.save()

viewlog1 = cmod.ViewedLog()
viewlog1.user = fomouser1
viewlog1.product = p2
viewlog1.save()

viewlog2 = cmod.ViewedLog()
viewlog2.user = fomouser1
viewlog2.product = p3
viewlog2.save()

viewlog3 = cmod.ViewedLog()
viewlog3.user = fomouser1
viewlog3.product = p4
viewlog3.save()

viewlog4 = cmod.ViewedLog()
viewlog4.user = fomouser1
viewlog4.product = p5
viewlog4.save()

viewlog5 = cmod.ViewedLog()
viewlog5.user = fomouser2
viewlog5.product = p6
viewlog5.save()

viewlog6 = cmod.ViewedLog()
viewlog6.user = fomouser2
viewlog6.product = p7
viewlog6.save()

viewlog7 = cmod.ViewedLog()
viewlog7.user = fomouser2
viewlog7.product = p8
viewlog7.save()

viewlog8 = cmod.ViewedLog()
viewlog8.user = fomouser3
viewlog8.product = p9
viewlog8.save()

productcart = cmod.ProductCart()
productcart.user = fomouser1
productcart.product = p1
productcart.quantity = 1
productcart.save()

productcart1 = cmod.ProductCart()
productcart1.user = fomouser1
productcart1.product = p2
productcart1.quantity = 3
productcart1.save()

productcart2 = cmod.ProductCart()
productcart2.user = fomouser1
productcart2.product = p3
productcart2.quantity = 14
productcart2.save()

productcart3 = cmod.ViewedLog()
productcart3.user = fomouser1
productcart3.product = p4
productcart3.quantity = 10
productcart3.save()

productcart4 = cmod.ProductCart()
productcart4.user = fomouser1
productcart4.product = p5
productcart4.save()

productcart5 = cmod.ProductCart()
productcart5.user = fomouser2
productcart5.product = p6
productcart5.save()

productcart6 = cmod.ProductCart()
productcart6.user = fomouser2
productcart6.product = p7
productcart6.save()

productcart7 = cmod.ProductCart()
productcart7.user = fomouser2
productcart7.product = p8
productcart7.save()

productcart8 = cmod.ProductCart()
productcart8.user = fomouser3
productcart8.product = p9
productcart8.save()
