$(function(){
    $('#id_typeofproduct').change(function (){
        let typeofproduct = $('#id_typeofproduct').val();

        $('.hideable').closest('p').hide()
            if(typeofproduct == 'bulk')
            {
                $('#id_quantity').closest('p').show();
                $('#id_reorder_trigger').closest('p').show();
                $('#id_reorder_quantity').closest('p').show();

            }
            else
            {
                $('#id_serial_number').closest('p').show();
            }
        });

        $('#id_typeofproduct').change();
    });
