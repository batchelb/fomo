from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required
from django.forms.widgets import TextInput


@view_function
@login_required
@permission_required('add_product', raise_exception = True )
def process_request(request):


    if request.method == 'POST':
        form=addProductForm(request.POST)
        if form.is_valid():
            print ('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',form.cleaned_data['typeofproduct'])
            if form.cleaned_data['typeofproduct'] == 'unique':
                product = cmod.UniqueProduct()
            elif form.cleaned_data['typeofproduct'] == 'rental':
                product = cmod.RentalProduct()
            elif form.cleaned_data['typeofproduct'] == 'bulk':
                product = cmod.BulkProduct()

            product.name = form.cleaned_data['name']
            product.category = form.cleaned_data['category']
            product.price = form.cleaned_data['price']

            if form.cleaned_data['typeofproduct'] == 'unique' or form.cleaned_data['typeofproduct'] == 'rental':
                product.serial_number = form.cleaned_data['serial_number']
            elif form.cleaned_data['typeofproduct'] == 'bulk':
                product.quantity = form.cleaned_data['quantity']
                product.reorder_trigger = form.cleaned_data['reorder_trigger']
                product.reorder_quantity = form.cleaned_data['reorder_quantity']



            product.save()
        print(1)
        return HttpResponseRedirect('/manager/viewproducts/')
    else:

        form = addProductForm()

        context = {

        'form':form,
        }

        return dmp_render(request,'addproduct.html',context)



class addProductForm(forms.Form):
    Product_Choices = [
        ['unique','Serialized Product'],
        ['bulk', 'Bulk Product'],
        ['rental','Rental Product']
    ]

    name = forms.CharField(label='Product Name', max_length=100)
    category = forms.ModelChoiceField(label='Category', queryset = cmod.Category.objects.all())
    typeofproduct = forms.ChoiceField(label='type of product', choices= Product_Choices)
    price = forms.DecimalField(label='Price')
    quantity = forms.IntegerField(label='Quanity', required = False, widget=forms.NumberInput(attrs={'forproducttype':'bulk', 'class': 'hideable'}))
    reorder_trigger = forms.IntegerField(label='Reorder trigger', required = False, widget=forms.NumberInput(attrs={'forproducttype':'bulk', 'class': 'hideable'}))
    reorder_quantity = forms.IntegerField(label='Reorder Quantity', required = False, widget=forms.NumberInput(attrs={'forproducttype':'bulk', 'class': 'hideable'}))
    serial_number = forms.CharField(label='Serial Number', required = False, widget=forms.TextInput(attrs={'forproducttype':'serialized', 'class': 'hideable'}))


    def clean_name(self):
        name = self.cleaned_data.get('name')
        return name
    def clean_category(self):
        category = self.cleaned_data.get('category')
        return category
    def clean_typeofproduct(self):
        typeofproduct = self.cleaned_data.get('typeofproduct')
        return typeofproduct
    def clean_price(self):
        price = self.cleaned_data.get('price')
        return price
    def clean_quantity(self):
        quantity = self.cleaned_data.get('quantity')
        return quantity
    def clean_reorder_trigger(self):
        reorder_trigger = self.cleaned_data.get('reorder_trigger')
        return reorder_trigger
    def clean_reorder_quantity(self):
        reorder_quantity = self.cleaned_data.get('reorder_quantity')
        return reorder_quantity
    def clean_serial_number(self):
        serial_number = self.cleaned_data.get('serial_number')
        return serial_number
