from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from accounts import models as amod
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import authenticate

@view_function
@login_required
@permission_required('change_fomouser', raise_exception = True )
def process_request(request):

    try:
        user = amod.FOMOUser.objects.get(id=request.urlparams[0])
        print(user.first_name)
    except amod.FOMOUser.DoesNotExist:
        print("!!!!!!!!!!!!!!!!!!!")
        return HttpResponseRedirect('/manager/viewusers/')

    print("Above post")
    if request.method == 'POST':

        form=ChangePasswordForm(request.POST)
        form.Request = user
        if form.is_valid():
            print(">>>>>>>>>>>>>>>>Form is Vaild")
            user.set_password(form.cleaned_data['newpassword'])
            user.save()

            print(1)
            return HttpResponseRedirect('/manager/viewusers/')
        else:
            return dmp_render(request,'changepassword.html',{'form' : form,})

    else:
        print("wrong")

        form = ChangePasswordForm()

        print('!!!!!!!!!!!!>>>>>>>>>>>>>',form)
        context = {
        'form':form,
        }

        return dmp_render(request,'changepassword.html',context)

class ChangePasswordForm(forms.Form):
    oldpassword = forms.CharField(label='Old Passwords', max_length=100)
    newpassword = forms.CharField(label='New Password', max_length=100)
    newpassword2 = forms.CharField(label='Confirm New Password', max_length=100)
    Request = None

    def clean_oldpassword(self):
        oldpassword = self.cleaned_data.get('oldpassword')
        return oldpassword
    def clean_newpassword(self):
        newpassword = self.cleaned_data.get('newpassword')
        return newpassword
    def clean_newpassword2(self):
        newpassword2 = self.cleaned_data.get('newpassword2')
        return newpassword2
    def clean(self):
        print(self.Request.username)
        print('>>>>>>>>>>>>>>>>>')
        user = authenticate(username=self.Request.username,password=self.cleaned_data.get('oldpassword'))

        if user is None:
            raise forms.ValidationError("That's not your  current password")

        if self.cleaned_data.get('newpassword2') != self.cleaned_data.get('newpassword'):
            raise forms.ValidationError("Passwords do not match")
        return self.cleaned_data
