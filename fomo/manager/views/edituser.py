from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from accounts import models as amod
from django.contrib.auth.decorators import login_required, permission_required

@view_function
@login_required
@permission_required('change_fomouser', raise_exception = True )
def process_request(request):

    try:
        user = amod.FOMOUser.objects.get(id=request.urlparams[0])
        print(user.first_name)
    except amod.FOMOUser.DoesNotExist:
        print("!!!!!!!!!!!!!!!!!!!")
        return HttpResponseRedirect('/manager/viewusers/')


    print("Above post")
    if request.method == 'POST':

        form=FOMOUserForm(request.POST)
        form.user = user
        if form.is_valid():
            print(">>>>>>>>>>>>>>>>Form is Vaild")
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.username = form.cleaned_data['username']
            user.email = form.cleaned_data['email']
            user.streetaddress = form.cleaned_data['streetaddress']
            user.city = form.cleaned_data['city']
            user.state = form.cleaned_data['state']
            user.postalcode = form.cleaned_data['postalcode']
            user.gender = form.cleaned_data['gender']
            user.birthdate = form.cleaned_data['birthdate']
            user.save()



            print(1)
            return HttpResponseRedirect('/manager/viewusers/')
        else:
            return dmp_render(request,'edituser.html',{'form' : form,})

    else:
        print("wrong")

        form = FOMOUserForm(initial = {'last_name':user.last_name, 'first_name':user.first_name, 'username':user.username, 'email':user.email, 'streetaddress':user.streetaddress, 'city':user.city, 'state':user.state, 'postalcode': user.postalcode, 'birthdate' : user.birthdate})

        print('!!!!!!!!!!!!>>>>>>>>>>>>>',form)
        context = {
        'user':user,
        'form':form,
        }

        return dmp_render(request,'edituser.html',context)

class FOMOUserForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)
    username = forms.CharField(label='Username', max_length=100)
    email = forms.CharField(label='Email', max_length=100)
    streetaddress = forms.CharField(label='Street Address', max_length=100)
    city = forms.CharField(label='city', max_length=100)
    state = forms.CharField(label='state', max_length=100)
    postalcode = forms.CharField(label='postalcode', max_length=100)
    gender = forms.ChoiceField(label='Gender', choices=[['True','Male'],['False','Female']])
    birthdate = forms.DateField(label='Birthdate')
    user = None

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        return first_name
    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        return last_name
    def clean_username(self):
        username = self.cleaned_data.get('username')
        return username
    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email
    def clean_streetaddress(self):
        streetaddress = self.cleaned_data.get('streetaddress')
        return streetaddress
    def clean_city(self):
        city = self.cleaned_data.get('city')
        return city
    def clean_state(self):
        state = self.cleaned_data.get('state')
        return state
    def clean_postalcode(self):
        postalcode = self.cleaned_data.get('postalcode')
        return postalcode
    def clean_birthdate(self):
        birthdate = self.cleaned_data.get('birthdate')
        return birthdate
    def clean_gender(self):
        gender = self.cleaned_data.get('gender')
        return gender
    def clean(self):
        requestedusername = self.cleaned_data.get('username')
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print(requestedusername)
        print(self.user.id)
        print(len(amod.FOMOUser.objects.filter(username = requestedusername).exclude(id=self.user.id)))

        if len(amod.FOMOUser.objects.filter(username = requestedusername).exclude(id=self.user.id)) > 0:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('stop')
            raise forms.ValidationError('username already taken')

        return self.cleaned_data






###########################################################33

@view_function
@login_required
@permission_required('delete_fomouser', raise_exception = True )
def delete(request):
        try:
            user = amod.FOMOUser.objects.get(id=request.urlparams[0])
            print(user.first_name)
        except cmod.Product.DoesNotExist:
            print("!!!!!!!!!!!!!!!!!!!")
            return HttpResponseRedirect('/manager/viewusers/')

        user.delete()

        return HttpResponseRedirect('/manager/viewusers/')
