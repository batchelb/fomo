from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from accounts import models as amod
from django.contrib.auth.decorators import login_required, permission_required
from django.forms.widgets import TextInput


@view_function
@login_required
@permission_required('dumb', raise_exception = True )
def process_request(request):
    return dmp_render(request,'index.html')
