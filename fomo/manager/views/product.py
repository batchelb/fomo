from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required



@view_function
@login_required
@permission_required('change_product', raise_exception = True )
def process_request(request):

        try:
            product = cmod.Product.objects.get(id=request.urlparams[0])
            print(product.name)
        except cmod.Product.DoesNotExist:
            print("!!!!!!!!!!!!!!!!!!!")
            return HttpResponseRedirect('/manager/viewproducts/')

        print("Above post")
        if request.method == 'POST':
            if hasattr(product, 'serial_number'):
                form=EditUniqueProductForm(request.POST)

            elif hasattr(product, 'quantity'):
                form = EditBulkProductForm(request.POST)

            if form.is_valid():
                print(">>>>>>>>>>>>>>>>Form is Vaild")
                product.name = form.cleaned_data['name']
                product.category = form.cleaned_data['category']
                product.price = form.cleaned_data['price']
                if hasattr(product, 'serial_number'):
                    product.serial_number = form.cleaned_data['serial_number']
                elif hasattr(product, 'quantity'):
                    product.quantity = form.cleaned_data['quantity']
                    product.reorder_trigger = form.cleaned_data['reorder_trigger']
                    product.reorder_trigger = form.cleaned_data['reorder_trigger']


            product.save()
            print(1)
            return HttpResponseRedirect('/manager/viewproducts/')
        else:
            print("wrong")
            if hasattr(product, 'serial_number'):
                form = EditUniqueProductForm( initial = {'name':product.name, 'category':product.category, 'price':product.price, 'serial_number':product.serial_number})

            elif hasattr(product, 'quantity'):
                form = EditBulkProductForm(initial = {'name':product.name, 'category':product.category, 'price':product.price, 'quantity': product.quantity, 'reorder_trigger': product.reorder_trigger, 'reorder_quantity': product.reorder_quantity})

            context = {
            'product':product,
            'form':form,
            }

            return dmp_render(request,'editproduct.html',context)



class EditBulkProductForm(forms.Form):
    name = forms.CharField(label='Product Name', max_length=100)
    category = forms.ModelChoiceField(label='Category', queryset = cmod.Category.objects.all())
    price = forms.DecimalField(label='Price')
    quantity = forms.IntegerField(label='Quanity')
    reorder_trigger = forms.IntegerField(label='Reorder trigger')
    reorder_quantity = forms.IntegerField(label='Reorder Quantity')

class EditUniqueProductForm(forms.Form):
    name = forms.CharField(label='Product Name', max_length=100)
    category = forms.ModelChoiceField(label='Category', queryset = cmod.Category.objects.all())
    price = forms.DecimalField(label='Price')
    serial_number = forms.CharField(label='Serial Number')

class EditRentalProductForm(forms.Form):
    name = forms.CharField(label='Product Name', max_length=100)
    category = forms.ModelChoiceField(label='Category', queryset = cmod.Category.objects.all())
    price = forms.DecimalField(label='Price')
    serial_number = forms.CharField(label='Serial Number')
    # def clean_name(self):
    #     name = self.cleaned_data.get('name')
    #     parts = name.strip().split()
    #     if len(parts) <= 1:
    #         raise forms.ValidationError('Please enter both first and last name')
    #     return name





    #############################################################################################
#### Delete of products

@view_function
@login_required
@permission_required('delete_products', raise_exception = True )
def delete(request):

        try:
            product = cmod.Product.objects.get(id=request.urlparams[0])
        except cmod.Product.DoesNotExist:
            return HttpResponseRedirect('/manager/viewproducts/')
        name = product.name
        product.delete()
        context={
        'name': name
        }
        return HttpResponseRedirect('/manager/viewproducts/', context)
