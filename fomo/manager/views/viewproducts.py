from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from catalog import models as cmod
from django.contrib.auth.decorators import login_required, permission_required

@view_function
@login_required
@permission_required('see_products', raise_exception = True )
def process_request(request):
    #query all products
    products = cmod.Product.objects.order_by('name').all()


    print('context?')
    context = {
    'products': products,
    }
    print('>>>>',products)
    return dmp_render(request, 'viewproducts.html', context)

    # try:
    #     .get(id=234234)
    # except cmod.Product.DoesNotExist:
    #     return HttpResponseRedirect('/manager/product/')




############################################33
@view_function
@login_required
@permission_required('see_products', raise_exception = True )
def getquantity(request):
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
        print(product.name)
    except cmod.Product.DoesNotExist:
        print("!!!!!!!!!!!!!!!!!!!")
        return HttpResponse('Failer')

    return HttpResponse(product.quantity)
