from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from accounts import models as amod
from django.contrib.auth.decorators import login_required, permission_required

@view_function
@login_required
@permission_required('see_fomousers', raise_exception = True )
def process_request(request):

    users = amod.FOMOUser.objects.order_by('last_name').all()
    print('here')
    print(users)
    print('>>>>>>>>>>>>>>>>')
    context = {
     'users' : users,
    }

    return dmp_render(request, 'viewusers.html', context)
